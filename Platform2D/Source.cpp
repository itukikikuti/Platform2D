#include "XLibrary11.hpp"
using namespace XLibrary11;

int Main()
{
	Camera camera;

	Sprite playerSprite(L"player.png");
	Sprite platformSprite(L"platform.png");

	playerSprite.scale = Float3(2.0f, 2.0f, 2.0f);
	platformSprite.scale = Float3(2.0f, 2.0f, 2.0f);

	Float3 playerPos;
	Float3 playerSpeed;
	Float3 platformPos[3];

	platformPos[0] = Float3(0.0f, -100.0f, 0.0f);
	platformPos[1] = Float3(200.0f, -40.0f, 0.0f);
	platformPos[2] = Float3(-200.0f, 100.0f, 0.0f);

	while (Refresh())
	{
		camera.Update();

		if (Input::GetKey(VK_LEFT))
		{
			playerSpeed.x = -5.0f;
		}

		if (Input::GetKey(VK_RIGHT))
		{
			playerSpeed.x = 5.0f;
		}

		if (Input::GetKeyDown(VK_SPACE))
		{
			playerSpeed.y = 15.0f;
		}

		playerSpeed.x *= 0.9f;
		playerSpeed.y -= 1.0f;

		for (int i = 0; i < 3; i++)
		{
			Float3 now = playerPos;
			Float3 next = playerPos + playerSpeed;

			if (platformPos[i].x - 116.0f < next.x &&
				platformPos[i].x + 116.0f > next.x &&
				platformPos[i].y - 46.0f < now.y &&
				platformPos[i].y + 46.0f > now.y)
			{
				if (playerSpeed.x < 0.0f)
				{
					playerSpeed.x = 0.0f;
					playerPos.x = platformPos[i].x + 116.0f;
				}
				if (playerSpeed.x > 0.0f)
				{
					playerSpeed.x = 0.0f;
					playerPos.x = platformPos[i].x - 116.0f;
				}
			}

			if (platformPos[i].x - 116.0f < now.x &&
				platformPos[i].x + 116.0f > now.x &&
				platformPos[i].y - 46.0f < next.y &&
				platformPos[i].y + 46.0f > next.y)
			{
				if (playerSpeed.y < 0.0f)
				{
					playerSpeed.y = 0.0f;
					playerPos.y = platformPos[i].y + 46.0f;
				}
				if (playerSpeed.y > 0.0f)
				{
					playerSpeed.y = 0.0f;
					playerPos.y = platformPos[i].y - 46.0f;
				}
			}

			//if (platformPos[i].x - 116.0f < playerPos.x &&
			//	platformPos[i].x + 116.0f > playerPos.x &&
			//	platformPos[i].y - 46.0f < playerPos.y &&
			//	platformPos[i].y + 46.0f > playerPos.y)
			//{
			//	if (playerSpeed.y < 0.0f)
			//	{
			//		playerSpeed.y = 0.0f;
			//		playerPos.y = platformPos[i].y + 46.0f;
			//	}
			//	if (playerSpeed.y > 0.0f)
			//	{
			//		playerSpeed.y = 0.0f;
			//		playerPos.y = platformPos[i].y - 46.0f;
			//	}
			//	if (playerSpeed.x < 0.0f)
			//	{
			//		playerSpeed.x = 0.0f;
			//		playerPos.x = platformPos[i].x + 116.0f;
			//	}
			//	if (playerSpeed.x > 0.0f)
			//	{
			//		playerSpeed.x = 0.0f;
			//		playerPos.x = platformPos[i].x - 116.0f;
			//	}
			//}

			platformSprite.position = platformPos[i];
			platformSprite.Draw();
		}

		playerPos += playerSpeed;

		playerSprite.position = playerPos;
		playerSprite.Draw();

	}
}
